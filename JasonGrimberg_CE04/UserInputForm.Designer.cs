﻿namespace JasonGrimberg_CE04
{
    partial class UserInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInputForm));
            this.gbStudent = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblStudent = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.numAge = new System.Windows.Forms.NumericUpDown();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbFristName = new System.Windows.Forms.TextBox();
            this.cbStudent = new System.Windows.Forms.ComboBox();
            this.tsUserInputForm = new System.Windows.Forms.ToolStrip();
            this.uibtnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnExitNewStudent = new System.Windows.Forms.Button();
            this.gbStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).BeginInit();
            this.tsUserInputForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbStudent
            // 
            this.gbStudent.Controls.Add(this.btnExitNewStudent);
            this.gbStudent.Controls.Add(this.btnClear);
            this.gbStudent.Controls.Add(this.lblGender);
            this.gbStudent.Controls.Add(this.lblStudent);
            this.gbStudent.Controls.Add(this.lblAge);
            this.gbStudent.Controls.Add(this.lblLastName);
            this.gbStudent.Controls.Add(this.lblFirstName);
            this.gbStudent.Controls.Add(this.numAge);
            this.gbStudent.Controls.Add(this.rbFemale);
            this.gbStudent.Controls.Add(this.rbMale);
            this.gbStudent.Controls.Add(this.tbLastName);
            this.gbStudent.Controls.Add(this.tbFristName);
            this.gbStudent.Controls.Add(this.cbStudent);
            this.gbStudent.Location = new System.Drawing.Point(11, 27);
            this.gbStudent.Margin = new System.Windows.Forms.Padding(2);
            this.gbStudent.Name = "gbStudent";
            this.gbStudent.Padding = new System.Windows.Forms.Padding(2);
            this.gbStudent.Size = new System.Drawing.Size(213, 178);
            this.gbStudent.TabIndex = 1;
            this.gbStudent.TabStop = false;
            this.gbStudent.Text = "Student";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(4, 148);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(65, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(19, 107);
            this.lblGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(48, 13);
            this.lblGender.TabIndex = 4;
            this.lblGender.Text = "Gender: ";
            // 
            // lblStudent
            // 
            this.lblStudent.AutoSize = true;
            this.lblStudent.Location = new System.Drawing.Point(17, 87);
            this.lblStudent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStudent.Name = "lblStudent";
            this.lblStudent.Size = new System.Drawing.Size(50, 13);
            this.lblStudent.TabIndex = 4;
            this.lblStudent.Text = "Student: ";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(36, 66);
            this.lblAge.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(32, 13);
            this.lblAge.TabIndex = 4;
            this.lblAge.Text = "Age: ";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(4, 45);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(64, 13);
            this.lblLastName.TabIndex = 4;
            this.lblLastName.Text = "Last Name: ";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(4, 25);
            this.lblFirstName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(63, 13);
            this.lblFirstName.TabIndex = 4;
            this.lblFirstName.Text = "First Name: ";
            // 
            // numAge
            // 
            this.numAge.Location = new System.Drawing.Point(71, 64);
            this.numAge.Margin = new System.Windows.Forms.Padding(2);
            this.numAge.Name = "numAge";
            this.numAge.Size = new System.Drawing.Size(127, 20);
            this.numAge.TabIndex = 3;
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(71, 127);
            this.rbFemale.Margin = new System.Windows.Forms.Padding(2);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(59, 17);
            this.rbFemale.TabIndex = 6;
            this.rbFemale.TabStop = true;
            this.rbFemale.Text = "Female";
            this.rbFemale.UseVisualStyleBackColor = true;
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Location = new System.Drawing.Point(71, 107);
            this.rbMale.Margin = new System.Windows.Forms.Padding(2);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(48, 17);
            this.rbMale.TabIndex = 5;
            this.rbMale.TabStop = true;
            this.rbMale.Text = "Male";
            this.rbMale.UseVisualStyleBackColor = true;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(71, 44);
            this.tbLastName.Margin = new System.Windows.Forms.Padding(2);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(128, 20);
            this.tbLastName.TabIndex = 2;
            // 
            // tbFristName
            // 
            this.tbFristName.Location = new System.Drawing.Point(71, 23);
            this.tbFristName.Margin = new System.Windows.Forms.Padding(2);
            this.tbFristName.Name = "tbFristName";
            this.tbFristName.Size = new System.Drawing.Size(128, 20);
            this.tbFristName.TabIndex = 1;
            // 
            // cbStudent
            // 
            this.cbStudent.FormattingEnabled = true;
            this.cbStudent.Items.AddRange(new object[] {
            "Yes - Full Time",
            "Yes - Part Time",
            "No"});
            this.cbStudent.Location = new System.Drawing.Point(71, 85);
            this.cbStudent.Margin = new System.Windows.Forms.Padding(2);
            this.cbStudent.Name = "cbStudent";
            this.cbStudent.Size = new System.Drawing.Size(128, 21);
            this.cbStudent.TabIndex = 4;
            // 
            // tsUserInputForm
            // 
            this.tsUserInputForm.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsUserInputForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uibtnAdd});
            this.tsUserInputForm.Location = new System.Drawing.Point(0, 0);
            this.tsUserInputForm.Name = "tsUserInputForm";
            this.tsUserInputForm.Size = new System.Drawing.Size(234, 25);
            this.tsUserInputForm.TabIndex = 2;
            this.tsUserInputForm.Text = "tsAdd";
            // 
            // uibtnAdd
            // 
            this.uibtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.uibtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("uibtnAdd.Image")));
            this.uibtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uibtnAdd.Name = "uibtnAdd";
            this.uibtnAdd.Size = new System.Drawing.Size(33, 22);
            this.uibtnAdd.Text = "&Add";
            this.uibtnAdd.Click += new System.EventHandler(this.uibtnAdd_Click);
            // 
            // btnExitNewStudent
            // 
            this.btnExitNewStudent.Location = new System.Drawing.Point(144, 148);
            this.btnExitNewStudent.Margin = new System.Windows.Forms.Padding(2);
            this.btnExitNewStudent.Name = "btnExitNewStudent";
            this.btnExitNewStudent.Size = new System.Drawing.Size(65, 23);
            this.btnExitNewStudent.TabIndex = 7;
            this.btnExitNewStudent.Text = "Exit";
            this.btnExitNewStudent.UseVisualStyleBackColor = true;
            this.btnExitNewStudent.Click += new System.EventHandler(this.btnExitNewStudent_Click);
            // 
            // UserInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 215);
            this.ControlBox = false;
            this.Controls.Add(this.tsUserInputForm);
            this.Controls.Add(this.gbStudent);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(250, 253);
            this.MinimumSize = new System.Drawing.Size(250, 253);
            this.Name = "UserInputForm";
            this.ShowIcon = false;
            this.Text = "New Student";
            this.Load += new System.EventHandler(this.UserInputForm_Load);
            this.gbStudent.ResumeLayout(false);
            this.gbStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).EndInit();
            this.tsUserInputForm.ResumeLayout(false);
            this.tsUserInputForm.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbStudent;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblStudent;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.NumericUpDown numAge;
        private System.Windows.Forms.RadioButton rbFemale;
        private System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.TextBox tbFristName;
        private System.Windows.Forms.ComboBox cbStudent;
        private System.Windows.Forms.ToolStrip tsUserInputForm;
        private System.Windows.Forms.ToolStripButton uibtnAdd;
        private System.Windows.Forms.Button btnExitNewStudent;
    }
}