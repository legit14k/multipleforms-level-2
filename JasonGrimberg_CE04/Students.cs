﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE04
{
    public class Students
    {
        // All student variables
        string firstName;
        string lastName;
        decimal age;
        string status;
        int statusIndex;
        bool male;
        bool female;

        // Student constructors
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public decimal Age { get => age; set => age = value; }
        public string Status { get => status; set => status = value; }
        public int StatusIndex { get => statusIndex; set => statusIndex = value; }
        public bool Male { get => male; set => male = value; }
        public bool Female { get => female; set => female = value; }

        // String override to display in listview
        public override string ToString()
        {
            // Return only a few variables
            return $"Name: {FirstName} {LastName} Age: {Age}";
        }
    }
}
