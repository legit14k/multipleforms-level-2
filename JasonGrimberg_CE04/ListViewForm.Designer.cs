﻿namespace JasonGrimberg_CE04
{
    partial class ListViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListViewForm));
            this.lvListViewForm = new System.Windows.Forms.ListView();
            this.tsListViewWindow = new System.Windows.Forms.ToolStrip();
            this.lvbtnClear = new System.Windows.Forms.ToolStripButton();
            this.tsListViewWindow.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvListViewForm
            // 
            this.lvListViewForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvListViewForm.Location = new System.Drawing.Point(0, 31);
            this.lvListViewForm.Margin = new System.Windows.Forms.Padding(2);
            this.lvListViewForm.MultiSelect = false;
            this.lvListViewForm.Name = "lvListViewForm";
            this.lvListViewForm.Size = new System.Drawing.Size(284, 231);
            this.lvListViewForm.TabIndex = 1;
            this.lvListViewForm.UseCompatibleStateImageBehavior = false;
            // 
            // tsListViewWindow
            // 
            this.tsListViewWindow.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsListViewWindow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lvbtnClear});
            this.tsListViewWindow.Location = new System.Drawing.Point(0, 0);
            this.tsListViewWindow.Name = "tsListViewWindow";
            this.tsListViewWindow.Size = new System.Drawing.Size(284, 31);
            this.tsListViewWindow.TabIndex = 2;
            this.tsListViewWindow.Text = "Menu";
            // 
            // lvbtnClear
            // 
            this.lvbtnClear.Image = ((System.Drawing.Image)(resources.GetObject("lvbtnClear.Image")));
            this.lvbtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.lvbtnClear.Name = "lvbtnClear";
            this.lvbtnClear.Size = new System.Drawing.Size(62, 28);
            this.lvbtnClear.Text = "&Clear";
            this.lvbtnClear.Click += new System.EventHandler(this.lvbtnClear_Click);
            // 
            // ListViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ControlBox = false;
            this.Controls.Add(this.lvListViewForm);
            this.Controls.Add(this.tsListViewWindow);
            this.MaximizeBox = false;
            this.Name = "ListViewForm";
            this.ShowIcon = false;
            this.Text = "List View";
            this.Load += new System.EventHandler(this.ListViewForm_Load);
            this.tsListViewWindow.ResumeLayout(false);
            this.tsListViewWindow.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvListViewForm;
        private System.Windows.Forms.ToolStrip tsListViewWindow;
        private System.Windows.Forms.ToolStripButton lvbtnClear;
    }
}