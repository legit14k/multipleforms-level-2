﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JasonGrimberg_CE04
{
    public partial class ListViewForm : Form
    {
        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        //public EventHandler ObjectAdded;

        // -----------------------------------------------------------------------------
        // Initialize ListViewForm
        // -----------------------------------------------------------------------------
        public ListViewForm()
        {
            InitializeComponent();
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        // ListView Form load trigger
        private void ListViewForm_Load(object sender, EventArgs e)
        {
            UserInputForm uif = new UserInputForm();

            // Sub to the event handler for the object added
            uif.ObjectAdded = new EventHandler(ObjectAddedHandler);
        }

        private void lvbtnClear_Click(object sender, EventArgs e)
        {
            lvListViewForm.Items.Clear();
        }

        // New object added handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Set the input form as the sender
            UserInputForm userForm = sender as UserInputForm;

            // Set the Student Data as the main form student data
            Students s = userForm.Data;

            // Create a new list to put objects into
            ListViewItem lvi = new ListViewItem();

            // Set the new list items as strings
            lvi.Text = s.ToString();

            // Set the tag as the student data
            lvi.Tag = s;

            // Add the new object to the list
            lvListViewForm.Items.Add(lvi);
        }
        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Hide the second form method
        public void HideWindow()
        {
            // Hide the second form when the 
            // display check mark is not visible
            Hide();
        }

        
    }
}
