﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/// <summary>
/// Jason Grimberg
/// CE04
/// Main form with list view and new windows page
/// </summary>
namespace JasonGrimberg_CE04
{
    public partial class MainForm : Form
    {
        ListViewForm lvf = new ListViewForm();

        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        public EventHandler SelectionChanged;

        // -----------------------------------------------------------------------------
        // Student data components
        // -----------------------------------------------------------------------------
        // New Students selected object
        public Students SelectedObject
        {
            // Get what the user has selected
            get
            {
                // Make sure that the user has selected an object
                if (lvObjectList.SelectedItems.Count > 0)
                {
                    // Only return the first selected object
                    return lvObjectList.SelectedItems[0].Tag as Students;
                }
                else
                {
                    // Return nothing when the user has not selected anything
                    return new Students();
                }
            }
        }

        // -----------------------------------------------------------------------------
        // Initialize MainForm
        // -----------------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();
        }

        // -----------------------------------------------------------------------------
        // Change Events
        // -----------------------------------------------------------------------------
        // Selection changed event
        private void lvObjectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Invoke the selection to the current selection
            //SelectionChanged?.Invoke(this, new EventArgs());
        }

        // Check the state of the display check mark
        private void menuDisplay_CheckedChanged(object sender, EventArgs e)
        {
            // Once the check is not there it will hide the second screen
            if (menuDisplay.Checked != true)
            {
                // Calling the method to hide the window
                lvf.HideWindow();
            }
        }

        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // Add new object event handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Set the input form as the sender
            UserInputForm userForm = sender as UserInputForm;
            
            // Set the Student Data as the main form student data
            Students s = userForm.Data;

            // Create a new list to put objects into
            ListViewItem lvi = new ListViewItem();

            // Set the new list items as strings
            lvi.Text = s.ToString();

            // Set the tag as the student data
            lvi.Tag = s;

            // Add the new object to the list
            lvObjectList.Items.Add(lvi);
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        // MainForm load trigger
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Call the new window count method
            WindowCount();
            // Call the new object count method
            ObjectCount();
        }

        // Add new window from the main form
        private void btnNewWindow_Click(object sender, EventArgs e)
        {
            UserInputForm uif = new UserInputForm();

            // Sub to the event handler for the object added
            uif.ObjectAdded = new EventHandler(ObjectAddedHandler);
            
            // Show a new input window
            uif.Show();

            // Call the new window count method
            WindowCount();
            // Call the new object count method
            ObjectCount();
        }

        // Double click event for the list view
        private void lvObjectList_DoubleClick(object sender, EventArgs e)
        {
            // New instance of the user input form
            UserInputForm uif = new UserInputForm();

            // Set the selection to the new window
            SelectionChanged = new EventHandler(uif.SelectionChangedHandler);

            // Invoke the selection to call the new data coming into the new window
            SelectionChanged?.Invoke(this, new EventArgs());

            // Show the user input data window
            uif.Show();
        }

        // Refresh the count when the window is activated
        private void MainForm_Activated(object sender, EventArgs e)
        {
            // Call the new window count method
            WindowCount();
            // Call the new object count method
            ObjectCount();
        }

        // Ask the user if they meant to hit the exit button
        private void menuExit_Click(object sender, EventArgs e)
        {
            // Set variable for the dialog result and give the user information on what is going on
            DialogResult dialogResult = MessageBox.Show("Closing will delete any changes that were made.",
                "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dialogResult == DialogResult.Yes)
            {
                // Exit the application if the answer is yes
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                // Return and don't exit
                return;
            }
        }
        
        // Display the listview form with the check mark
        private void menuDisplay_Click(object sender, EventArgs e)
        {
            // If the display is checked then it will do nothing
            if (menuDisplay.Checked == true)
            {
                // Showing the second form with data
                lvf.Show();
            }
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // How many windows open
        public void WindowCount()
        {
            // Set variable for the new windows that are opened
            int formCount = Application.OpenForms.OfType<UserInputForm>().Count();

            // Set the text box to the new count of the windows
            tbWindowsOpen.Text = formCount.ToString();
        }

        // How many object are in the listview
        public void ObjectCount()
        {
            // set variable for the objects in the lisview
            int objectCount = lvObjectList.Items.Count;

            // Set the text box as the new count of the objects
            tbListObjects.Text = objectCount.ToString();
        }

        // Clear the listview and reset the count
        private void menuClear_Click(object sender, EventArgs e)
        {
            lvObjectList.Items.Clear();
            WindowCount();
            ObjectCount();
        }
    }
}
