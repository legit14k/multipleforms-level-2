﻿namespace JasonGrimberg_CE04
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMainInfo = new System.Windows.Forms.GroupBox();
            this.btnNewWindow = new System.Windows.Forms.Button();
            this.lblUserObjects = new System.Windows.Forms.Label();
            this.lblUserInputWindows = new System.Windows.Forms.Label();
            this.tbListObjects = new System.Windows.Forms.TextBox();
            this.tbWindowsOpen = new System.Windows.Forms.TextBox();
            this.lvObjectList = new System.Windows.Forms.ListView();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuList = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDisplay = new System.Windows.Forms.ToolStripMenuItem();
            this.menuClear = new System.Windows.Forms.ToolStripMenuItem();
            this.gbMainInfo.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMainInfo
            // 
            this.gbMainInfo.Controls.Add(this.btnNewWindow);
            this.gbMainInfo.Controls.Add(this.lblUserObjects);
            this.gbMainInfo.Controls.Add(this.lblUserInputWindows);
            this.gbMainInfo.Controls.Add(this.tbListObjects);
            this.gbMainInfo.Controls.Add(this.tbWindowsOpen);
            this.gbMainInfo.Controls.Add(this.lvObjectList);
            this.gbMainInfo.Location = new System.Drawing.Point(16, 40);
            this.gbMainInfo.Name = "gbMainInfo";
            this.gbMainInfo.Size = new System.Drawing.Size(376, 278);
            this.gbMainInfo.TabIndex = 1;
            this.gbMainInfo.TabStop = false;
            this.gbMainInfo.Text = "List";
            // 
            // btnNewWindow
            // 
            this.btnNewWindow.Location = new System.Drawing.Point(6, 215);
            this.btnNewWindow.Name = "btnNewWindow";
            this.btnNewWindow.Size = new System.Drawing.Size(122, 42);
            this.btnNewWindow.TabIndex = 3;
            this.btnNewWindow.Text = "New Window";
            this.btnNewWindow.UseVisualStyleBackColor = true;
            this.btnNewWindow.Click += new System.EventHandler(this.btnNewWindow_Click);
            // 
            // lblUserObjects
            // 
            this.lblUserObjects.AutoSize = true;
            this.lblUserObjects.Location = new System.Drawing.Point(194, 234);
            this.lblUserObjects.Name = "lblUserObjects";
            this.lblUserObjects.Size = new System.Drawing.Size(71, 20);
            this.lblUserObjects.TabIndex = 2;
            this.lblUserObjects.Text = "Objects: ";
            // 
            // lblUserInputWindows
            // 
            this.lblUserInputWindows.AutoSize = true;
            this.lblUserInputWindows.Location = new System.Drawing.Point(184, 202);
            this.lblUserInputWindows.Name = "lblUserInputWindows";
            this.lblUserInputWindows.Size = new System.Drawing.Size(81, 20);
            this.lblUserInputWindows.TabIndex = 2;
            this.lblUserInputWindows.Text = "Windows: ";
            // 
            // tbListObjects
            // 
            this.tbListObjects.Location = new System.Drawing.Point(272, 231);
            this.tbListObjects.Name = "tbListObjects";
            this.tbListObjects.ReadOnly = true;
            this.tbListObjects.Size = new System.Drawing.Size(100, 26);
            this.tbListObjects.TabIndex = 1;
            // 
            // tbWindowsOpen
            // 
            this.tbWindowsOpen.Location = new System.Drawing.Point(272, 198);
            this.tbWindowsOpen.Name = "tbWindowsOpen";
            this.tbWindowsOpen.ReadOnly = true;
            this.tbWindowsOpen.Size = new System.Drawing.Size(100, 26);
            this.tbWindowsOpen.TabIndex = 1;
            // 
            // lvObjectList
            // 
            this.lvObjectList.Location = new System.Drawing.Point(6, 25);
            this.lvObjectList.MultiSelect = false;
            this.lvObjectList.Name = "lvObjectList";
            this.lvObjectList.Size = new System.Drawing.Size(366, 169);
            this.lvObjectList.TabIndex = 0;
            this.lvObjectList.UseCompatibleStateImageBehavior = false;
            this.lvObjectList.View = System.Windows.Forms.View.List;
            this.lvObjectList.SelectedIndexChanged += new System.EventHandler(this.lvObjectList_SelectedIndexChanged);
            this.lvObjectList.DoubleClick += new System.EventHandler(this.lvObjectList_DoubleClick);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuList});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(399, 33);
            this.mainMenuStrip.TabIndex = 2;
            this.mainMenuStrip.Text = "mainMenuStrip";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(50, 29);
            this.menuFile.Text = "&File";
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(123, 30);
            this.menuExit.Text = "E&xit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // menuList
            // 
            this.menuList.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuDisplay,
            this.menuClear});
            this.menuList.Name = "menuList";
            this.menuList.Size = new System.Drawing.Size(50, 29);
            this.menuList.Text = "&List";
            // 
            // menuDisplay
            // 
            this.menuDisplay.CheckOnClick = true;
            this.menuDisplay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuDisplay.Name = "menuDisplay";
            this.menuDisplay.Size = new System.Drawing.Size(154, 30);
            this.menuDisplay.Text = "&Display";
            this.menuDisplay.CheckedChanged += new System.EventHandler(this.menuDisplay_CheckedChanged);
            this.menuDisplay.Click += new System.EventHandler(this.menuDisplay_Click);
            // 
            // menuClear
            // 
            this.menuClear.Name = "menuClear";
            this.menuClear.Size = new System.Drawing.Size(154, 30);
            this.menuClear.Text = "&Clear";
            this.menuClear.Click += new System.EventHandler(this.menuClear_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 305);
            this.Controls.Add(this.mainMenuStrip);
            this.Controls.Add(this.gbMainInfo);
            this.Location = new System.Drawing.Point(200, 200);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(421, 361);
            this.MinimumSize = new System.Drawing.Size(421, 361);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main List Form";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.gbMainInfo.ResumeLayout(false);
            this.gbMainInfo.PerformLayout();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMainInfo;
        private System.Windows.Forms.Button btnNewWindow;
        private System.Windows.Forms.Label lblUserObjects;
        private System.Windows.Forms.Label lblUserInputWindows;
        private System.Windows.Forms.TextBox tbListObjects;
        private System.Windows.Forms.TextBox tbWindowsOpen;
        public System.Windows.Forms.ListView lvObjectList;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem menuList;
        private System.Windows.Forms.ToolStripMenuItem menuDisplay;
        private System.Windows.Forms.ToolStripMenuItem menuClear;
    }
}

