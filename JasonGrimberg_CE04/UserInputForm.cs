﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JasonGrimberg_CE04
{
    public partial class UserInputForm : Form
    {
        ListViewForm lvf = new ListViewForm();

        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        public EventHandler ObjectAdded;
        public EventHandler ListObjectAdded;

        // -----------------------------------------------------------------------------
        // Student data components
        // -----------------------------------------------------------------------------
        // New student data
        public Students Data
        {
            // Get all of the student data
            get
            {
                Students s = new Students();
                s.FirstName = tbFristName.Text;
                s.LastName = tbLastName.Text;
                s.Age = numAge.Value;
                s.Status = cbStudent.Text;
                s.StatusIndex = cbStudent.SelectedIndex;
                s.Male = rbMale.Checked;
                s.Female = rbFemale.Checked;
                return s;
            }

            // Set all of the student data
            set
            {
                tbFristName.Text = value.FirstName;
                tbLastName.Text = value.LastName;
                numAge.Value = value.Age;
                cbStudent.Text = value.Status;
                rbMale.Checked = value.Male;
                rbFemale.Checked = value.Female;
            }
        }

        // -----------------------------------------------------------------------------
        // Initialize UserInputForm
        // -----------------------------------------------------------------------------
        public UserInputForm()
        {
            InitializeComponent();
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        // User input form load trigger
        private void UserInputForm_Load(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            ListViewForm lvf = new ListViewForm();

            // Sub to the added handler on second form
            ObjectAdded += main.ObjectAddedHandler;
            ListObjectAdded += lvf.ObjectAddedHandler;

            // Sub to the selection change handler on the second form
            main.SelectionChanged += SelectionChangedHandler;

            main.WindowCount();
            main.ObjectCount();

        }

        // Add the current windows input into the list on main form
        private void uibtnAdd_Click(object sender, EventArgs e)
        {
            // Add to the main form list view
            AddToList();

            // Add to list view on ListView Form
            AddToListView();

            // Clear the data from the user inputs
            Data = new Students();
        }

        private void btnExitNewStudent_Click(object sender, EventArgs e)
        {
            // Close current window
            Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            // Clear the data in all input controls
            Data = new Students();
        }

        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // Selection changed handler
        public void SelectionChangedHandler(object sender, EventArgs e)
        {
            MainForm main = sender as MainForm;

            // Setting the selection from second screen to the data that is on the screen
            Data = main.SelectedObject;
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Add to both lists method
        public void AddToList()
        {
            // Check all of the input controls to make sure there is nothing empty
            if (tbFristName.Text != "" && tbLastName.Text != "" && numAge.Value != 0 
                && cbStudent.SelectedIndex != -1)
            {
                // Object added to second form with null check
                if (ObjectAdded != null)
                {
                    // Invoke the object added method
                    ObjectAdded(this, new EventArgs());
                }
            }
            else
            {
                // Tell the user what is going on and why they need to 
                // fill out all of the information
                MessageBox.Show("Please fill out all information.", "Doh!", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        public void AddToListView()
        {
            // Check all of the input controls to make sure there is nothing empty
            if (tbFristName.Text != "" && tbLastName.Text != "" && numAge.Value != 0
                && cbStudent.SelectedIndex != -1)
            {
                // Object added to second form with null check
                if (ListObjectAdded != null)
                {
                    // Invoke the object added method
                    ListObjectAdded(this, new EventArgs());
                }
            }

        }
    }
}
